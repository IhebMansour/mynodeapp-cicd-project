# MyNodeApp CI/CD Project

## Project Overview

Welcome to the MyNodeApp CI/CD project! This repository contains the configuration for the GitLab CI/CD pipeline, designed to streamline the development, testing, and deployment processes of the MyNodeApp application.

## Pipeline Workflow

The CI/CD pipeline has multiple stages to ensure a smooth development lifecycle:

1. **Test Stage (`test`):**
   - Executes unit tests for the MyNodeApp application.
   - Caches dependencies to optimize subsequent test runs.
   - Generates a JUnit XML report for test results.

2. **Build Stage (`build`):**
   - Builds a Docker image for the MyNodeApp application.
   - Tags the image with the version from the `package.json` file and the CI/CD pipeline instance ID.
   - Publishes version information as an artifact.

3. **Deploy to Development (`deploy_dev`):**
   - Deploys the MyNodeApp to the development environment.
   - Utilizes a Docker Compose file (`docker-compose.yaml`).
   - Sets environment-specific variables.
   - Executes deployment on the specified development server.

4. **Functional Tests (`run_functional_tests`):**
   - Runs functional tests on the deployed development environment.

5. **Deploy to Staging (`deploy_staging`):**
   - Deploys the MyNodeApp to the staging environment.
   - Depends on the success of the development deployment and functional tests.
   - Sets environment-specific variables.
   - Executes deployment on the specified staging server.

6. **Performance Tests (`run_performance_tests`):**
   - Runs performance tests on the deployed staging environment.

7. **Deploy to Production (`deploy_prod`):**
   - Manually triggered deployment to the production environment.
   - Depends on the success of the staging deployment and performance tests.
   - Sets environment-specific variables.
   - Executes deployment on the specified production server.

## Environment Variables

The pipeline uses the following environment variables:

- `IMAGE_NAME`: The name of the Docker image.
- `IMAGE_TAG`: The version tag for the Docker image.
- `DEV_SERVER_HOST`: Development server host IP address.
- `DEV_ENDPOINT`: Development environment endpoint.
- `STAGING_SERVER_HOST`: Staging server host IP address.
- `STAGING_ENDPOINT`: Staging environment endpoint.
- `PROD_SERVER_HOST`: Production server host IP address.
- `PROD_ENDPOINT`: Production environment endpoint.

## Manual Deployment to Production

To manually deploy to the production environment:

1. Navigate to the GitLab project.
2. Go to "CI / CD" > "Pipelines."
3. Find the relevant pipeline and click on the "deploy_to_prod" job.
4. Click "Run" to manually trigger the deployment to the production environment.

## Security Notes

- The pipeline includes a static application security testing (SAST) job (`sast`) for security analysis.
- Ensure the security of sensitive information such as private keys and credentials.

Feel free to customize the pipeline configuration and environment variables to suit the needs of your MyNodeApp project. Happy coding!